package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IMedicoDAO;
import com.mitocode.model.Medico;
import com.mitocode.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService{
	
	@Autowired
	private IMedicoDAO dao;
	
	@Override
	public void registrar(Medico medico) {
		dao.save(medico);	
	}

	@Override
	public void modificar(Medico medico) {
		dao.save(medico);
	}

	@Override
	public void eliminar(Medico medico) {
		dao.delete(medico);		
	}

	@Override
	public Medico listarId(int idMedico) {
		Medico medico = new Medico();
		Optional<Medico> optionalMedico = dao.findById(idMedico);
		if (optionalMedico.isEmpty()) {
			medico = null;
			return medico;
		}
		medico = optionalMedico.get();
		return medico;
	}

	@Override
	public List<Medico> listar() {
		return dao.findAll();
	}
	
}
