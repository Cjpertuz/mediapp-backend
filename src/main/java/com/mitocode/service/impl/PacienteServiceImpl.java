package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IPacienteDAO;
import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService {

	@Autowired
	private IPacienteDAO dao;
	
	@Override
	public Paciente registrar(Paciente paciente) {
		return dao.save(paciente);
	}

	@Override
	public Paciente modificar(Paciente paciente) {
		return dao.save(paciente);		
	}

	@Override
	public void eliminar(Paciente paciente) {
		dao.delete(paciente);
		
	}

	@Override
	public Paciente listarId(int idPaciente) {
		Paciente paciente = new Paciente();
		Optional<Paciente> optionalPaciente =  dao.findById(idPaciente);
		if (optionalPaciente.isEmpty()) {
			paciente = null;
			return paciente;
		}
		paciente = optionalPaciente.get();
		return paciente;
	}

	@Override
	public List<Paciente> listar() {
		return dao.findAll();
	}
	
}
