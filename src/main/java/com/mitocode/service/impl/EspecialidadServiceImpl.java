package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IEspecialidadDAO;
import com.mitocode.model.Especialidad;
import com.mitocode.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {
	
	@Autowired
	private IEspecialidadDAO dao;
	
	@Override
	public void registrar(Especialidad especialidad) {
		dao.save(especialidad);	
	}

	@Override
	public void modificar(Especialidad especialidad) {
		dao.save(especialidad);
	}

	@Override
	public void eliminar(Especialidad especialidad) {
		dao.delete(especialidad);	
	}

	@Override
	public Especialidad listarId(int idEspecialidad) {
		Especialidad especialidad = new Especialidad();
		Optional<Especialidad> optionalEspecialidad = dao.findById(idEspecialidad);
		if (optionalEspecialidad.isEmpty()) {
			especialidad = null;
			return especialidad;
		}
		especialidad = optionalEspecialidad.get();
		return especialidad;
	}

	@Override
	public List<Especialidad> listar() {
		return dao.findAll();
	}

}
