package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IConsultaDAO;
import com.mitocode.dao.IConsultaExamenDAO;
import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.model.Consulta;
import com.mitocode.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService {
	
	@Autowired
	private IConsultaDAO dao;
	
	@Autowired
	private IConsultaExamenDAO ceDAO;
	
	@Transactional
	@Override
	public Consulta registrar(ConsultaListaExamenDTO consultaDTO) {
		//consultaDTO.getDetalleConsulta().forEach(x -> x.setConsulta(consulta));
		//return dao.save(consultaDTO);
		Consulta cons = new Consulta();
		consultaDTO.getConsulta().getDetalleConsulta().forEach(x -> x.setConsulta(consultaDTO.getConsulta()));
		cons = dao.save(consultaDTO.getConsulta());
		
		consultaDTO.getListExamen().forEach(e -> ceDAO.registrar(consultaDTO.getConsulta().getIdConsulta(), e.getIdExamen()));
		return cons;
	}

	@Override
	public void modificar(Consulta consulta) {
		dao.save(consulta);
		
	}

	@Override
	public void eliminar(Consulta consulta) {
		dao.delete(consulta);
		
	}

	@Override
	public Consulta listarId(int idConsulta) {
		Consulta consulta = new Consulta();
		Optional<Consulta> optionalConsulta = dao.findById(idConsulta);
		if (optionalConsulta.isEmpty()) {
			consulta = null;
			return consulta;
		}
		consulta = optionalConsulta.get();
		return consulta;
	}

	@Override
	public List<Consulta> listar() {
		return dao.findAll();
	}

}
