package com.mitocode.service;

import java.util.List;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.model.Consulta;

public interface IConsultaService {

	Consulta registrar(ConsultaListaExamenDTO consultaDTO);
	
	void modificar(Consulta consulta);
	
	void eliminar(Consulta consulta);
	
	Consulta listarId(int idConsulta);
	
	List<Consulta> listar();
}
